# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital: 
- Use case user
- Use case manajemen perusahaan
- Use case direksi perusahaan (dashboard, monitoring, analisis)

    - Berikut beberapa use case yang saya buat :

        https://docs.google.com/spreadsheets/d/1tQDMi6-ByJtS0aDMw63pwitW5nO951GtYBLpKXLV7Hc/edit?usp=sharing

       
        ![dok_pbo](dok_pbo/complitnya.gif)

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital
- Berikut class diagram yang saya buat :

    ![dok_pbo](dok_pbo/class_diagram.png)

- Berikut class diagram menggunakan mermaid.js:
```mermaid    
classDiagram

class MyIm3App {
    - String nomor
    - String username
    - String password
    - String email
    + int pulsa
    + int kuota
    + main args()
}

class User {
    - String nomor
    - String username
    - String password
    - String email
    + login()
    + profil()
}

class Apps {
    + int pulsa
    + int kuota
    + clearscreen()
    + cekPulsa()
    + cekKuotaData()
    + pembelianPulsa()
    + pembelianPaketData()
}

class Registrasi {
    + login()
    + profil()
}

MyIm3App <|-- User
MyIm3App <|-- Apps
Registrasi <|-- User
```


# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
**SOLID** merupakan akronim yang terdiri dari lima prinsip desain perangkat lunak yang dikembangkan untuk membantu dalam membangun sistem yang mudah dipahami, fleksibel, dan mudah untuk dipelihara. Berikut ini adalah penjelasan singkat tentang setiap poin dari **SOLID Design Principle** beserta contoh penerapannya:

1. **Single Responsibility Principle (SRP)** - Prinsip Tanggung Jawab Tunggal
   Prinsip ini menyatakan bahwa sebuah kelas seharusnya hanya memiliki satu tanggung jawab atau alasan untuk berubah. Dengan kata lain, sebuah kelas seharusnya hanya memiliki satu fokus utama.
   
   Contoh penerapan:
   Misalkan kita memiliki kelas "Customer" yang bertanggung jawab untuk menyimpan data pelanggan dan juga mengirim notifikasi kepada pelanggan. Penerapan SRP akan memisahkan tanggung jawab ini menjadi dua kelas terpisah, yaitu "Customer" yang hanya menangani data pelanggan dan "NotificationSender" yang bertanggung jawab untuk mengirim notifikasi.

2. **Open/Closed Principle (OCP)** - Prinsip Terbuka/Tertutup
   Prinsip ini menyatakan bahwa entitas perangkat lunak (kelas, modul, dll.) seharusnya terbuka untuk ekstensi (open for extension) namun tertutup untuk modifikasi (closed for modification). Artinya, ketika ada perubahan atau penambahan fitur, seharusnya tidak perlu mengubah kode yang sudah ada, tetapi cukup menambahkan kode baru.

   Contoh penerapan:
   Misalkan kita memiliki kelas "Shape" dengan metode "calculateArea". Jika ingin menambahkan jenis bentuk baru, misalnya "Rectangle", kita tidak perlu mengubah kelas "Shape". Sebaliknya, kita dapat membuat kelas "Rectangle" yang mengimplementasikan "Shape" dan menambahkan logika perhitungan luas khusus untuk bentuk persegi panjang tersebut.

3. **Liskov Substitution Principle (LSP)** - Prinsip Penggantian Liskov
   Prinsip ini menyatakan bahwa objek dari suatu kelas turunan seharusnya bisa digunakan sebagai pengganti objek kelas induk tanpa mengganggu kebenaran atau perilaku program.

   Contoh penerapan:
   Misalkan kita memiliki kelas "Square" yang merupakan turunan dari kelas "Rectangle". Menurut LSP, objek "Square" seharusnya bisa digunakan sebagai pengganti objek "Rectangle" tanpa mengubah logika yang bergantung pada kelas "Rectangle". Jika metode "setHeight" dipanggil pada objek "Square", seharusnya tidak mengubah nilai lebar yang sudah diatur sebelumnya.

4. **Interface Segregation Principle (ISP)** - Prinsip Pemisahan Antarmuka
   Prinsip ini menyatakan bahwa klien seharusnya tidak dipaksa untuk bergantung pada antarmuka yang tidak mereka gunakan. Dalam hal ini, lebih baik memiliki beberapa antarmuka khusus yang melayani kebutuhan spesifik, daripada satu antarmuka besar yang mencoba memenuhi semua kebutuhan.

   Contoh penerapan:
   Misalkan kita memiliki antarmuka "Printer" dengan metode "print" yang digunakan oleh berbagai jenis printer. Namun, tidak semua jenis printer mendukung semua fitur yang ada pada antarmuka tersebut, misalnya metode "scan". Lebih baik memisahkan antarmuka "Printer" menjadi antarmuka "Printable" dan antarmuka "Scannable" agar klien hanya bergantung pada antarmuka yang sesuai dengan kebutuhan mereka.

5. **Dependency Inversion Principle (DIP)** - Prinsip Inversi Ketergantungan
   Prinsip ini menyatakan bahwa kelas-kelas tingkat tinggi seharusnya tidak bergantung pada kelas-kelas tingkat rendah, tetapi keduanya seharusnya bergantung pada abstraksi. Dalam kata lain, ketergantungan antara kelas seharusnya bergantung pada kontrak atau antarmuka, bukan implementasi yang spesifik.

   Contoh penerapan:
   Misalkan kita memiliki kelas "OrderProcessor" yang bergantung pada kelas "PaymentProcessor". Dalam penerapan yang buruk, "OrderProcessor" langsung bergantung pada kelas "PaymentProcessor" yang spesifik. Dengan menerapkan DIP, "OrderProcessor" seharusnya bergantung pada antarmuka "PaymentProcessorInterface", dan implementasi spesifik dari antarmuka tersebut akan diinjeksi melalui konstruktor atau metode lainnya.

Semua prinsip SOLID ini bekerja bersama-sama untuk menciptakan desain yang kuat, terstruktur, dan mudah dikelola. Dengan menerapkan prinsip-prinsip ini, kita dapat membangun perangkat lunak yang lebih mudah dipahami, fleksibel, dan mudah untuk diperluas atau diperbaiki di masa depan.

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
    
#### saya berencana menggunakan OBSERVER DESIGN PATTERN

**Observer Design Pattern** merupakan salah satu design pattern yang digunakan untuk mengatur hubungan antara objek-objek dalam sistem. Pattern ini memungkinkan objek yang disebut "subject" (subjek) untuk mempertahankan daftar objek yang disebut "observers" (pengamat) dan memberi tahu mereka secara otomatis ketika terjadi perubahan pada subjek tersebut. Observers akan merespons perubahan tersebut dengan melakukan tindakan yang sesuai.

Dalam **Observer Design Pattern**, subjek dan pengamat saling terlepas satu sama lain. Subjek tidak bergantung pada pengamat secara spesifik, melainkan hanya pada antarmuka umum yang diimplementasikan oleh pengamat. Ini memungkinkan fleksibilitas dalam menambahkan atau menghapus pengamat tanpa mempengaruhi subjek atau pengamat lainnya.

Secara umum, **Observer Design Pattern** terdiri dari komponen-komponen berikut:

- **Subject**: Merepresentasikan subjek yang mengirim notifikasi perubahan. Biasanya memiliki metode untuk menambahkan, menghapus, dan memberi tahu pengamat.
- **Observer**: Merepresentasikan pengamat yang menerima notifikasi perubahan dari subjek. Biasanya memiliki metode yang akan dipanggil oleh subjek saat perubahan terjadi.

lalu mengapa saya memilih Design Pattern ini, salah 2 alasannya adalah :
1. **Memudahkan identifikasi perubahan dan responsnya**:

    Ketika melakukan reverse engineering terhadap aplikasi myIM3, Observer pattern membantu dalam mengidentifikasi komponen yang berperan sebagai subjek yang mengalami perubahan dan komponen yang berperan sebagai pengamat yang merespons perubahan tersebut. Dengan memahami perubahan yang terjadi dan respons yang diberikan oleh pengamat, kita dapat mengidentifikasi bagian-bagian kode yang relevan dan memahami bagaimana interaksi antara komponen-komponen tersebut terjadi.

2. **Memahami proses notifikasi**:

    Dalam aplikasi web seperti myIM3, ada situasi di mana perubahan pada satu komponen perlu diinformasikan kepada komponen-komponen lainnya. Observer pattern membantu dalam menganalisis implementasi notifikasi yang terjadi dalam aplikasi myIM3. Dengan memahami bagaimana proses notifikasi terjadi, termasuk pengiriman informasi perubahan dan penerimaan oleh pengamat, kita dapat mengidentifikasi proses komunikasi yang terjadi dalam aplikasi tersebut.

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 

# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

# 10. Bonus: Mendemonstrasikan penggunaan Machine Learning

